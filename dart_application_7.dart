import 'dart:io';

const Map currencySell = {
  'USD': 84.50,
  'EURO': 87.24,
  'RUB': 1.1900,
  'KZT': 0.1857
};
const Map currencyBuy = {
  'USD': 81.70,
  'EURO': 84.20,
  'RUB': 1.0145,
  'KZT': 0.1234
};
void main(List<String> args) {
  print('Актуальный курс');
  infoSell();
  infoBuy();
  print(
      '\n1-Хотите обменять  на сом(Покупка)!\n2-Хотите обменять сом (Продажа)!');
  String choise = stdin.readLineSync()!;
  if (choise == '1') {
    bought();
  } else if (choise == '2') {
    toSell();
  }
}

infoSell() {
  print('Продажа');
  currencySell.forEach((key, value) {
    print('$key: $value');
  });
}

infoBuy() {
  print('Покупка');
  currencyBuy.forEach((key, value) {
    print('$key: $value');
  });
}

toSell() {
  String valute = ('Выберите валюту:\nUSD\nEUR\nRUB\nKZT');
  print(valute);

  String bill = '';
  double sum = 0;
  double result = 0;
  String choise = stdin.readLineSync()!;
  if (choise.toUpperCase() == 'USD') {
    print('сколько долларов  купить?');
    sum = currencySell['USD'];
    bill = 'USD';
  } else if (choise.toUpperCase() == 'EUR') {
    print('сколько евро  купить?');
    sum = currencySell['EURO'];
    bill = 'EURO';
  } else if (choise.toUpperCase() == 'RUB') {
    print('сколько рублей  купить?');
    sum = currencySell['RUB'];
    bill = 'RUB';
  } else if (choise.toUpperCase() == 'KZT') {
    print('сколько тенге  купить?');
    sum = currencySell['KZT'];
    bill = 'KZT';
  } else {
    print('Ошибка ');
    exit(0);
  }
  int money = int.parse(stdin.readLineSync()!);
  result = money.toDouble() * sum;
  print('$result сом на $money $bill');
  print('$result сом на $money ${choise.toUpperCase()}');
}

bought() {
  String valute = ('Какая у вас валюта:\nUSD\nEUR\nRUB\nKZT');
  print(valute);
  String bill = '';
  double result = 0;
  double sum = 0;
  String choise = stdin.readLineSync()!;
  if (choise.toUpperCase() == 'USD') {
    print('Сколько Долларов  продать?');
    sum = currencyBuy['USD'];
    bill = 'USD';
  } else if (choise.toUpperCase() == 'EUR') {
    print('Сколько Евро  продать?');
    sum = currencyBuy['EURO'];
    bill = 'EURO';
  } else if (choise.toUpperCase() == 'RUB') {
    print('Сколько Рублей  продать?');
    sum = currencyBuy['RUB'];
    bill = 'RUB';
  } else if (choise.toUpperCase() == 'KZT') {
    print('Сколько Тенге  продать?');
    sum = currencyBuy['KZT'];
    bill = 'KZT';
  } else {
    print('Ошибка');
    exit(0);
  }
  int money = int.parse(stdin.readLineSync()!);
  result = money.toDouble() * sum;
  print('Обмен: на $money $bill $result Сом');
  print('Обмен: на $money ${choise.toUpperCase()} $result Сом');
}
