void main(List<String> arguments) {
  print('ex1');
  List number = [1, 7, 12, 3, 56, 2, 87, 34, 5];
  print(number.first); //
  print(number[5]); //
  print(number.last); //

  print('ex2');
  List word = [3, 12, 43, 1, 25, 6, 5, 7];
  List word1 = [55, 11, 23, 56, 78, 1, 9];
  word.addAll(word1);
  print(word);

  print('ex3');
  List words = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(words.sublist(2, 9));

  print('ex4');
  List combo = [601123, 2, "dart", 45, 95, "dart24", 1];
  print(combo.contains('dart'));
  print(combo.contains(951));

  print('ex5');
  List number2 = ['post', 1, 0, 'flutter'];
  String mydart = 'Flutter';
  print(number2.contains(mydart.toLowerCase()));

  print('ex6');
  List symbols = ['I', 'Started', "Learn", "Flutter", "Since", "April"];
  String myFlutter = symbols.join('*');
  print(myFlutter);

  print('ex7');
  List exsort = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
  exsort.sort(((a, b) => a.compareTo(b)));
  print(exsort);
}
