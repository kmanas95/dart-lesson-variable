void main(List<String> arguments) {
  List<int> middleNum = [1, 2, 3, 4, 0, 5, 5, 7];
  List<int> length = [1, 5, 3, 2, 2, 5, 6, 1, 2, 3];
  List<dynamic> type = [1, '2', true, 1, 4, false, 'qwerty'];
  funcExersice1();
  funcExersice2(list: middleNum);
  funcExersice3(list: length);
  funcExersice4(a: type);
}

funcExersice1() {
  List<int> myList = [
    2,
    3,
    2,
    5,
    6,
    7,
    8,
    9,
  ];

  int myVariable = 0;
  bool isContains = false;

  myList.forEach((e) {
    if (myVariable == e + 1) {
      isContains = true;
    }
    myVariable = e;
  });

  if (isContains) {
    print('Yes');
  } else {
    print('No');
  }
}

double funcExersice2({required List list}) {
  double i = 0;
  list.forEach((element) {
    i += element;
  });
  i /= list.length;
  print('Middle number: $i');
  return i;
}

int funcExersice3({required List list}) {
  int i = 0;
  list.forEach((element) {
    i += 1;
  });
  print('lenght: $i');
  return i;
}

List<dynamic> funcExersice4({required List a}) {
  List type = [];
  a.forEach((element) {
    type.add(element.runtimeType);
  });
  print(type);
  return a;
}
