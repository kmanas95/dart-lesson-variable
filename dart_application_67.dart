void main(List<String> arguments) {
  List newlist = [];
  List a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
  List b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

  List total = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100];
  List save = [];

  String word = 'a';
  String text = 'dart';

  funcExersice1(newlist, a: a, b: b);
  funcExersice2(total: total, save: save);
  funcExersice3(word: word, text: text);
}

List<dynamic> funcExersice1(List newList, {required List a, required List b}) {
  for (int i = 0; i < a.length; i++) {
    if (b.contains(a[i]) == !newList.contains(a[i])) {
      newList.add(a[i]);
    }
  }
  print('same elements: $newList');
  return newList;
}

List<dynamic> funcExersice2({required List total, required List save}) {
  for (int i = 0; i < total.length; i++) {
    if (total[i].isEven) {
      save.add(total[i]);
    }
  }
  print('even: $save');
  return save;
}

int funcExersice3({required String word, required String text}) {
  int count = 0;
  for (int i = 0; i < text.length; i++) {
    if (word == text[i]) {
      count++;
    }
  }
  print(' Same letters: $count');
  return count;
}
